package com.realdolmen.moviedb

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class MovieListAdapter (private val list: ArrayList<Movie>,private val context: Context)
    : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(movie: Movie) {
            var title: TextView = itemView.findViewById(R.id.tv_title) as TextView
            var year: TextView = itemView.findViewById(R.id.tv_year) as TextView
            var poster: ImageView = itemView.findViewById(R.id.img_poster) as ImageView
            title.text = movie.title
            year.text = movie.year.toString()
            Picasso.get().load(movie.poster).into(poster);
            itemView.setOnClickListener {
                val intent = Intent(context, ViewDetails::class.java)
                intent.putExtra("id", movie.id)
                context.startActivity(intent)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MovieListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_layout, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return list.size
    }
    override fun onBindViewHolder(holder: MovieListAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }

}
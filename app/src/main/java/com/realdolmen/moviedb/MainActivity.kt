package com.realdolmen.moviedb

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://angularcoursebackend.azurewebsites.net/api/").build()

        var movieService = retrofit.create(MovieService::class.java)

        var movies = movieService.allMovies()
        movies.enqueue(object : Callback<List<Movie>>{
            override fun onResponse(call: Call<List<Movie>>, response: Response<List<Movie>>) {
                if (response.code() == 200) {
                    var adapter = MovieListAdapter(response.body() as ArrayList<Movie>, this@MainActivity)
                    var layoutManager = LinearLayoutManager(this@MainActivity)
                    rec.layoutManager = layoutManager
                    rec.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<List<Movie>>, t: Throwable) {
                println("null")
            }



        })

    }
}

package com.realdolmen.moviedb

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService{
    @GET ("movies?public=true")
    fun allMovies(): Call<List<Movie>>

    @GET("movies/{id}")
    fun getOne(@Path ("id")id : Int): Call<MovieDetails>
}
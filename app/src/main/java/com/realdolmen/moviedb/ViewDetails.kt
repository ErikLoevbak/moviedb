package com.realdolmen.moviedb

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_movie_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ViewDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        var retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://angularcoursebackend.azurewebsites.net/api/").build()

        var movieService = retrofit.create(MovieService::class.java)
        var call = movieService.getOne(intent.getIntExtra("id", 0))
        call.enqueue(object : Callback<MovieDetails>{
            override fun onFailure(call: Call<MovieDetails>, t: Throwable) {

            }

            override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>) {
                if (response.code() == 200) {
                    var movieDetails= response.body()!!
                    tv_title.text = movieDetails.title
                    tv_year.text = movieDetails.year.toString()
                    tv_runtime.text = movieDetails.runtime.toString() + " min"
                    tv_description.text = movieDetails.plot
                    Picasso.get().load(movieDetails.poster).into(img_poster)

                }
            }

        })
    }
}

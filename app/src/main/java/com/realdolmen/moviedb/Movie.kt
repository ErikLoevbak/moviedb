package com.realdolmen.moviedb


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Movie(
    @SerializedName("id")
    val id: Int,
    @SerializedName("poster")
    val poster: String,
    @SerializedName("seen")
    val seen: Boolean,
    @SerializedName("title")
    val title: String,
    @SerializedName("year")
    val year: Int
)
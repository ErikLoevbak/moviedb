package com.realdolmen.moviedb


import com.google.gson.annotations.SerializedName

data class MovieDetails(
    @SerializedName("id")
    val id: Int,
    @SerializedName("plot")
    val plot: String,
    @SerializedName("poster")
    val poster: String,
    @SerializedName("rating")
    val rating: Double,
    @SerializedName("runtime")
    val runtime: Int,
    @SerializedName("seen")
    val seen: Boolean,
    @SerializedName("title")
    val title: String,
    @SerializedName("year")
    val year: Int
)